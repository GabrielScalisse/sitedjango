from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.response import Response


User = get_user_model()


class StatisticView(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'charts.html', {"customers": 10})


def get_data(request, *args, **kwargs):
    data = {"01/10": 100, "05/10": 200, "10/10": 300, "15/10": 400}

    return JsonResponse(data)


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        qs_count = User.objects.all().count()
        labels = ["01/10", "05/10", "10/10", "15/10"]
        default_items = [qs_count, 200, 400, 600, 800, 1000]
        data = {
                "labels": labels,
                "default": default_items,
        }
        return Response(data)

